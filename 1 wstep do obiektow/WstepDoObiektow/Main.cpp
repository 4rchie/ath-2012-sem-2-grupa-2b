#include <string>
#include <iostream>
#include "Student.h"

using namespace std;

void staticPrintToScreen(Student student){
	cout << student.Name() << " " << student.Surname() << endl;
}

void printToScreen(Student* student){
	cout << student->Name() << " " << student->Surname() << endl;
}

int main(){
	cout << "programowanie obiektowe : wstep" << endl;

	Student staticStudent1("karol","statyczny");
	Student staticStudent2("marek","statyczny");

	Student* student1 = new Student("Karol","Kowalski");
	Student* student2 = new Student("Marek","Malinowski");
	
	staticPrintToScreen(staticStudent1);
	staticPrintToScreen(staticStudent2);
	printToScreen(student1);
	printToScreen(student2);

	student1->~Student();
	student2->~Student();

	system("pause");
	return 0;
}

