#pragma once
#include "IOsoba.h"

class Profesor : public IOsoba
{
	private:
		string _imie;
		string _nazwisko;

	public:
		Profesor(string,string);
		~Profesor(void);

		string IOsoba::ImieNazwisko();
};

