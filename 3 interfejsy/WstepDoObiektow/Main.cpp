#include <string>
#include <iostream>
#include "IOsoba.h"
#include "Student.h"
#include "Magister.h"
#include "Doktor.h"
#include "Profesor.h"


using namespace std;

void WypiszNaEkranie(IOsoba * osoba){
	cout << osoba->ImieNazwisko() << endl;
}


int main(){
	cout << "interfejsy" << endl;

	auto student = new Student("Jan","Kowalski");
	WypiszNaEkranie(student);

	auto magister = new Magister("Jan","Kowalski");
	WypiszNaEkranie(magister);

	auto doktor = new Doktor("Jan","Kowalski");
	WypiszNaEkranie(doktor);

	auto profesor = new Profesor("Jan","Kowalski");
	WypiszNaEkranie(profesor);


	system("pause");
	return 0;
}

