// Dziedziczenie.cpp : Defines the entry point for the console application.
//
#include <iostream>
#include "stdafx.h"
#include "Person.h"
#include "Student.h"

using namespace std;

int main(){
	auto person1 = new Person();	
	person1->SetName("Jan");
	person1->SetSurname("Kowalski");
	person1->WhoAreYou();
	person1->SayHello();

	auto person2 = new Student();	
	person2->SetName("Mateusz");
	person2->SetSurname("Kowalewski");
	person2->WhoAreYou();
	person2->SayHello();


	cout <<endl;

	Person* somePerson;

	somePerson = person1;
	somePerson->WhoAreYou();
	somePerson->SayHello();

	somePerson = person2;
	somePerson->WhoAreYou();
	somePerson->SayHello();

	cout <<endl;
	system("pause");
}