#pragma once
#include "bonus.h"
class MotivationBonus :	public Bonus
{
public:
	MotivationBonus(void);
	~MotivationBonus(void);

	string Bonus::Name();
	void Bonus::SetValue(int value);
	int Bonus::Value();
	float Bonus::Tax(); 

private:
	int _value;
};

