#include <string>

#include "Employee.h"
#include "Bonus.h"

Employee::Employee(void)
{
	_bonuses = new vector<Bonus*>();
}


Employee::~Employee(void)
{

}

void Employee::SetName(string name)
{
	_name = name;
}

string Employee::Name(){
	return _name;
}

void Employee::SetSurname(string surname)
{
	_surname = surname;
}

string Employee::Surname(){
	return _surname;
}


void Employee::SetBaseSalary(int baseSalary)
{
	_baseSalary = baseSalary;
}

float Employee::BaseSalary(){
	return _baseSalary;
}

vector<Bonus*>* Employee::Bonuses(){
	return _bonuses;
}