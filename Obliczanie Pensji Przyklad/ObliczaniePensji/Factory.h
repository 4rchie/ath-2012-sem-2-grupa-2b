#pragma once

#include "Employee.h"
#include "SalaryCalculator.h"

class Factory
{
public:
	Factory(void);
	~Factory(void);

	Employee* CreateEmployee();
	SalaryCalculator* CreateSalaryCalculator();
};

