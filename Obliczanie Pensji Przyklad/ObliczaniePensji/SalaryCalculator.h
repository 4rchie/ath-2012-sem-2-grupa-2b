#pragma once
#include "Employee.h"

class SalaryCalculator
{
public:
	SalaryCalculator(void);
	~SalaryCalculator(void);
	float Calculate(Employee* employee);
};

